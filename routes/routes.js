var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var
    config = require('../config/config'),
    path = require('path'),
    errors = require('../config/errors');

module.exports = function (app) {
    var auth = require('./auth');

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(expressValidator()); // this line must be immediately after any of the bodyParser middlewares!
    app.use(bodyParser.json());

    app.post('/signup',require('../controllers/auth').signup);
    app.post('/login',require('../controllers/auth').login);
    app.post('/forgotpassword',require('../controllers/auth').forgotpassword);

    app.get('/user/list', auth.checkApiToken, require('../controllers/user').list);
    app.post('/user/add', auth.checkApiToken,  require('../controllers/user').add);
    app.post('/user/edit', auth.checkApiToken,  require('../controllers/user').edit);
    app.post('/user/remove', auth.checkApiToken,  require('../controllers/user').remove);
    app.post('/user/status', auth.checkApiToken,  require('../controllers/user').status);
    
    app.route('/*').all(errors[404]);

};

