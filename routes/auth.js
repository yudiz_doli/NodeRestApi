'use strict';
var jwt = require('jsonwebtoken');
var errors = require('../config/errors'),
	config = require('../config/config');

module.exports.checkApiToken = function (req, res, next) {
	if (req.headers.authorization) {
		var token = req.headers.authorization;
		jwt.verify(token, config.secretKey, function (err, decoded) {
			if (err) {
				errors[401](req, res, next);
			} else {
				next();
			}
		});
	} else {
		errors[401](req, res, next);
	}
};

