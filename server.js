var http = require('http');
var express = require('express');
var app = express();
var env =  'development';

require('./routes/routes')(app);
var config = require('./config/config')[env];

app.listen(8080, function () {
  console.log("Example app listening ");
})