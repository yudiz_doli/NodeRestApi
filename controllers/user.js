'use strict';
var express = require('express');
var app = express();
var errors = require('../config/errors');
var config = require('../config/config');
var db = require('../config/mysql').db;

function list(req, res, next) {
    var sql = `CALL list()`;
    db.query(sql, true, function (err, result) {
        console.log(result)
        if (result.length > 0)
            res.status(200).json({ data: result[0] }).end();
        else
            res.status(400).json({ error: 'No user found' }).end();
    });
};
exports.list = list;

function add(req, res, next) {
    var sql = `CALL addUser('` + req.body.username + `','` + req.body.email + `','` + req.body.password + `')`;
    db.query(sql, true, function (err, result) {
        if (result && result.affectedRows)
            res.status(200).json({ message: 'User added successfully' }).end();
        else
            res.status(400).json({ error: 'Problem in adding user' }).end();
    });
};
exports.add = add;

function edit(req, res, next) {
    var sql = `CALL updateUser('` + req.body.username + `','` + req.body.email + `',` + req.body.id + `)`;
    db.query(sql, true, function (err, result) {
        if (result && result.affectedRows)
            res.status(200).json({ message: 'User updated successfully' }).end();
        else
            res.status(400).json({ error: 'Problem in updating user' }).end();
    });
};
exports.edit = edit;

function remove(req, res, next) {
    var sql = `CALL deleteUser(` + req.body.id + `)`;
    db.query(sql, true, function (err, result) {
        if (result && result.affectedRows)
            res.status(200).json({ message: 'User delete successfully' }).end();
        else
            res.status(400).json({ error: 'Problem in deleting user' }).end();
    });
};
exports.remove = remove;

function status(req, res, next) {
    var sql = `CALL changeStatus('` + req.body.status + `',` + req.body.id + `)`;
    db.query(sql, true, function (err, result) {
        if (result && result.affectedRows)
            res.status(200).json({ message: 'User status changed successfully' }).end();
        else
            res.status(400).json({ error: 'Problem in changing user status' }).end();
    });
};
exports.status = status;