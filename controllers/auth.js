'use strict';
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var express = require('express');
var app = express();
var errors = require('../config/errors');
var config = require('../config/config');
var db = require('../config/mysql').db;
var nodemailer = require('nodemailer');
var generator = require('generate-password');


function login(req, res, next) {
    var sql = `CALL login('` + req.body.username + `','` + req.body.password + `')`;
    db.query(sql, true, function (err, result) {
        console.log(result);
        if (result && result[0].length > 0) {

            // if user is found and password is right
            // create a token
            var token = jwt.sign(req.body.username, config.secretKey);

            res.status(200).json(
                {
                    message: 'Login Successfull',
                    data: result[0][0],
                    token: token
                }).end();
        } else
            res.status(400).json({ error: 'Invalid usename or password' }).end();
    });
};
exports.login = login;

function signup(req, res, next) {
    var sql = `CALL signup('` + req.body.username + `','` + req.body.email + `','` + req.body.password + `')`;
    db.query(sql, true, function (err, result) {
        if (result && result.affectedRows)
            res.status(200).json({ message: 'Signup successfully' }).end();
        else
            res.status(400).json({ error: 'Problem in signup' }).end();
    });
};
exports.signup = signup;

function forgotpassword(req, res, next) {

    req.checkBody({
        'email': {
            notEmpty: true,
            errorMessage: "This field is required"
        }
    });
    req.getValidationResult().then(function (result) {
        var errors = result.useFirstErrorOnly().mapped();
        if (!result.isEmpty()) {
            res.status(400).json({message: "Invalid parameter posted",error: errors});
        } else {
            var sql = `CALL getUserByEmail('` + req.body.email + `')`;
            db.query(sql, true, function (err, result) {
                if (result && result[0].length > 0) {

                    var transporter = nodemailer.createTransport(config.development.smtp);
                    var randomPassword = generator.generate({
                        length: 10,
                        numbers: true
                    });
                    var mailOptions = {
                        from: config.development.smtp.auth.user, 
                        to: req.body.email, 
                        subject: 'New password', 
                        html: 'New password :  <b>' + randomPassword + '</b>'
                    };
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return res.status(400).json({ error: error }).end();
                        } else {
                            var sql = `CALL changePassword(` + result[0][0].id + `,'` + randomPassword + `')`;
                            db.query(sql, true, function (err, result) {
                                if (result && result.affectedRows)
                                    res.status(200).json({ message: 'New password sent to your email id' }).end();
                                else
                                    res.status(400).json({ error: 'Problem in change password' }).end();
                            });
                        }
                    });
                }
                else {
                    res.status(400).json({ error: 'email id not exists' }).end();
                }
            });
        };
    });
}
exports.forgotpassword = forgotpassword;