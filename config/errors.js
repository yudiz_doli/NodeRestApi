//Error responses
// 200 OK
// 201 Created
// 202 Accepted
// 204 No Content
// 400 Bad Request
// 401 Unauthorized
// 403 Forbidden
// 404 Not Found
// 500 Internal Server Error
// 503 Service Unavailable
// 504 Gateway Timeout

'use strict';

module.exports[401] = function (req, res, next) {
  res.status(401).json({error: {code: 401, message: 'Unauthorized'}}).end();
};

module.exports[403] = function (req, res, next) {
  res.status(403).json({error: {code: 403, message:'Forbidden'}}).end();
};

module.exports[404] = function (req, res, next) {
  res.status(404).json({error: {code: 404, message: 'Not found'}}).end();
};

module.exports[400] = function (req, res, next) {
  res.status(400).json({error: {code: 400, message: 'Bad Request'}}).end();
};


