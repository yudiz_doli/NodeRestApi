var mysql = require('mysql');
var config = require('./config').development;

var con = mysql.createConnection({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.db,
    port: config.dbport
});

con.connect(function (err) {
    if (err)
        console.log("error" + err);
    else
        console.log("Connected!");
});

exports.db = con;